# Version Control for WordPress

This repository is a skeleton for repositories containing WordPress based sites. You may clone it and set the remote
path to your repository by running `git remote set-url origin <your repository URL>`.

## Requirements

[WP-CLI](https://github.com/wp-cli/wp-cli) is required alongside a CLI installation of PHP and MySQL.

:bulb: **Tip: You don't need to go through the process of building your own installation of PHP or MySQL for the command
line (unless you want to.)** If you have XAMPP/MAMP/WAMP or even just your system's native LAMP stack installed, you can
use any of those binaries by editing your `PATH`.

## Installation

After cloning the repository, edit `wp-dependencies-sample.php` with the proper values and rename it to `wp-dependencies.php`.

Use WP-CLI to get a working copy of WordPress in the directory:

```
wp core download
wp core config --dbname=database --dbuser=username --dbpass=password --dbhost=localhost
```

You may either use WP-CLI to complete the installation, or open it in the browser and install it manually.

Finally run `php wp-dependencies.php` to fetch and install any required themes and/or plugins.

## Plugins and Themes

Plugins and themes are ignored by the repository by default. Those that are necessary for the site to function should
be included in `wp-dependencies.php`.

Plugins and themes can be included in the repository as needed by updating `.gitignore`. To include either, add its path
to `.gitignore` with a preceding exclamation point. Example:

```
!wp-content/themes/ebsco-child
```

A plugin or theme should only be included in the repository when it isn't available via WordPress.org, or customizations
have been made to it. If customizations have been made to a plugin or theme that is available via WordPress.org then it's
preferable to only commit those files that have been updated, rather than the entire directory. This reduces the size of
our repositories and results in faster download times.
