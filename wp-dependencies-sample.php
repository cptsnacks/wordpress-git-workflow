<?php

/**
 * This is where you will declare your installation's dependencies.
 *
 * Items in either of these lists should correspond to the slug of the theme or plugin that you wish to install. The
 * dependency must be available via WordPress.org for the installer; custom or proprietary themes/plugins may be manually
 * added to the repository (refer to the readme file).
 */

$dependencies = array(
    'themes'  => array(),
    'plugins' => array()
);

# ---------------------------------------- #
# No need to edit anything below this line #
# lest it be in the spirit of hacking.     #
# ---------------------------------------- #

$ignoreThemes = $ignorePlugins = false;

if (php_sapi_name() !== 'cli') {
    exit;
}

array_shift($argv);

if (!empty($argv)) {
    foreach ($argv as $argument) {
        switch ($argument) {
            case '--ignore-themes':
                $ignoreThemes = true;
            break;

            case '--ignore-plugins':
                $ignorePlugins = true;
            break;

            case '--help':
                echo 'Usage: php wp-dependencies.php [--ignore-themes=false, --ignore-plugins=false, --help]', PHP_EOL, PHP_EOL;
                echo '--ignore-themes', PHP_EOL, "\tSkip the installation of themes when fetching dependencies", PHP_EOL, PHP_EOL;
                echo '--ignore-plugins', PHP_EOL, "\tSkip the installation of plugins when fetching dependencies", PHP_EOL, PHP_EOL;
                echo '--help', PHP_EOL, "\tDisplays this information", PHP_EOL;

                exit;
            break;
        }
    }
}

function install ($type) {
    global $dependencies;

    if (!array_key_exists($type, $dependencies)) {
        throw new Exception($type . ' is not a valid key in $dependencies');
    }

    foreach ($dependencies[$type] as $dependency) {
        echo 'Installing ' . $dependency . '...', PHP_EOL;

        exec(sprintf(
            'wp %s install %s',
            substr($type, 0, strlen($type) - 1), $dependency
        ));
    }
}

$ignoreThemes  ? null : install('themes');
$ignorePlugins ? null : install('plugins');
